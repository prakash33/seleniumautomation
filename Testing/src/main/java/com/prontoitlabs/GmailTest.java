package com.prontoitlabs;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GmailTest {
@Test
	public void createConnection()throws IOException, InterruptedException
	{
		try{
		//Create Connection
		System.setProperty("webdriver.chrome.driver", "/home/pronto/Desktop/chromedriver");
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		TimeOut.getInstance().timeOut(driver);
		driver.get("https://accounts.google.com/ServiceLogin?sacu=1&scc=1&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&osid=1&service=mail&ss=1&ltmpl=default&rm=false#identifier");	
		//Login to Gmail
		boolean flag =Login.getInstance().loginToGmail(driver);
		if (!flag){
			System.out.println("FAILED:createConnection");
			return;
		}
		//Logout from Gmail
		Logout.getInstance().logoutFromGmail(driver);
		TimeOut.getInstance().timeOut(driver);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);	
		TimeOut.getInstance().timeOut(driver);
		TimeOut.getInstance().timeOut(driver);
		//Print Result
		System.out.println("PASSED:createConnection");
		driver.close();
	}catch(Exception e)
		{
		System.out.println("FAILED:createConnection");
		}
	}
}
