package com.prontoitlabs;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Login {
	
	public String Email="Enter the Email";
	public String passwd="Enter the password";
	
	public static Login instance;
	
	public static Login getInstance(){
		if (instance == null){
			instance = new Login();
		}
		return instance;
	}
	public boolean loginToGmail(WebDriver driver)
	{
		try{
		WebElement email = driver.findElement(By.id("Email"));
		email.click();
		email.sendKeys(Email);
		TimeOut.getInstance().timeOut(driver);
		WebElement next = driver.findElement(By.id("next"));
		next.click();
		TimeOut.getInstance().timeOut(driver);
		WebElement password = driver.findElement(By.id("Passwd"));
		password.click();
		password.sendKeys(passwd);
		TimeOut.getInstance().timeOut(driver);
		WebElement signIn = driver.findElement(By.id("signIn"));
		signIn.click();
		TimeOut.getInstance().timeOut(driver);
		//Print Result
		System.out.println("PASSED:loginToGmail");
		return true;
	}catch(Exception e)
		{
		System.out.println("FAILED:loginToGmail");
		}
		return false;
		
	}
}
