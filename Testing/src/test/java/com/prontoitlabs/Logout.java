package com.prontoitlabs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Logout {

	public static Logout instance;
	
	public static Logout getInstance()
	{
		if(instance == null)
			instance = new Logout();
		return instance;
	}
	public void logoutFromGmail(WebDriver driver)
	{
		try{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
		WebElement logout = driver.findElement(By.cssSelector("#gb > div.gb_td.gb_oe > div.gb_La.gb_oe.gb_R.gb_ne.gb_T > div > div.gb_Ta.gb_Qb.gb_oe.gb_R.gb_cb > div.gb_1b.gb_Va.gb_oe.gb_R > a"));
		logout.click();
		TimeOut.getInstance().timeOut(driver);
		WebElement logoutFromGmail=driver.findElement(By.cssSelector("#gb_71"));
		logoutFromGmail.click();
		//Print Result
		System.out.println("PASSED:logoutFromGmail");
		}catch(Exception e)
		{
			System.out.println("FAILED:logoutFromGmail");
		}
	}
}